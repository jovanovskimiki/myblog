﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MvcMyBlog.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int PostID { get; set; }
        public DateTime CommentDate { get; set; }

        [Required(ErrorMessage = "Comment Content is required")]
        public string CommentContent { get; set; }

        [Required(ErrorMessage = "Full Name is required")]
        public string UserFullName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string UserEmail {get; set;}

    }
}