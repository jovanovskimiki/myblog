﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMyBlog.Models
{
    public class CategoryPost
    {
        public int CategoryPostID { get; set; }
        public string Category { get; set; }
    }
}